Feature: To test the chat bot interface

  @Sanity
  Scenario: Basic input to the chat bot
    Given user hits the base chat bot URL
    When user enters data into the input Hi box
    And user hits enter
    Then user receives a response back with Hello

  @ConversationStrict
  Scenario: To test a series of conversation with the bot strict check
    Given user hits the base chat bot URL
    When user enters the following data and hits enter
    #Enter Input Data Here
      | Hi            |
      | Hello         |
      | dude          |
      | Wassup        |
      | are you fine? |

    Then user receives the following response back
    #Expected response in the same order of questions asked as above
      | Hello                                                                                                                                                                |
      | Hello                                                                                                                                                                |
      | Vijay, I am happy to continue with this conversation. However, I would like to remind you that I am here to help you with Workplace tools usage. How can I help you? |
      | I am chatting with clients on the internet.                                                                                                                          |
      | OK. What would allow you to relax right now.                                                                                                                         |

  @ConversationRelaxed
  Scenario: To test a series of conversation with the bot relaxed check
    Given user hits the base chat bot URL
    When user enters the following data and hits enter
    #Enter Input Data Here
      | Hi            |
      | Hello         |
      | dude          |
      | Wassup        |
      | are you fine? |

    Then user receives the following response back Relaxed check
    #Expected response in the same order of questions asked as above. seperate multiple expected response with ","
      | Hello                                                                                                                                                                                    |
      | Hello, Hi there!                                                                                                                                                                         |
      | Vijay, I am happy to continue with this conversation. However, I would like to remind you that I am here to help you with Workplace tools usage. How can I help you?,OK. Glad you asked. |
      | I am chatting with clients on the internet.                                                                                                                                              |
      | OK. What would allow you to relax right now.                                                                                                                                             |

  @QAMap
  Scenario: To Pose Questions and expected answers columnWise
    Given user hits the base chat bot URL
    Then the below chat history is recorded
      | Hi            | Hello                                                                                                                                                                                    |
      | Hello         | Hello, Hi there!                                                                                                                                                                         |
      | dude          | Vijay, I am happy to continue with this conversation. However, I would like to remind you that I am here to help you with Workplace tools usage. How can I help you?,OK. Glad you asked. |
      | Wassup        | I am chatting with clients on the internet.                                                                                                                                              |
      | are you fine? | OK. What would allow you to relax right now.,OK. I won't say "yes" or "no" right now.                                                                                                    |


  user: Whats my name?
  agent:my name is Vijay
  agent: what would you like to know
  user: whats the weather?
    

  for(Sting q:ques){
  user: -- >
    agent agent : list<String> ---? maps value


  }
 # @Map2
 # Scenario: To Pose Questions and expected answers columnWise Map
 #   Given user hits the base chat bot URL
 #   Then the below chat history is recorded for users
 #     | Hi            | Hello                                                                                                                                                                                    | Hello                                    |
 #     | Hello         | Hello                                                                                                                                                                                    | Hi there!                                |
 #     | dude          | Vijay, I am happy to continue with this conversation. However, I would like to remind you that I am here to help you with Workplace tools usage. How can I help you?,OK. Glad you asked. | Hi                                       |
 #     | Wassup        | I am chatting with clients on the internet.                                                                                                                                              | Hello                                    |
 #     | are you fine? | OK. What would allow you to relax right now.                                                                                                                                             | OK. I won't say "yes" or "no" right now. |
 # cucumber.runtime.CucumberException: Can't convert DataTable to Map<class java.lang.String,java.util.List<java.lang.String>>
 # at cucumber.runtime.table.TableConverter.toMap(TableConverter.java:173)
