Feature: To test the new ChatBot

  @Sanity
  Scenario: TestBot Sanity Check
    Given  user navigates to the test bot url
    When user has sends a hi
    Then bot replies back

  @SingleBotOutput
  Scenario: To input texts rowWise with single output from the bot
    Given  user navigates to the test bot url
    Then user has the following conversation with Bot
      | User:Hi                                   |
      | Agent:Namaste                             |
      | Agent:am good, How are you?               |
      | Agent:thanks for chatting,have a good day |
      | User:Hello                                |
      | Agent:thanks for chatting,have a good day |
      | Agent:Namaste                             |
      | Agent:am good, How are you?               |
    And user inputs the values in the chatbot
    And user validates the chatbot output
    And user makes the call to chat base for Analytics


  @MultipleBotOutput
  Scenario: To input texts rowWise with multiple replies from the BOT
    Given  user navigates to the test bot url
    Then user has the following conversation with Bot
      | User:Hi                   |
      | Agent:Namaste             |
      | Agent:How was your day    |
      | User:Hello                |
      | Agent:thanks for chatting |
    And user inputs the values in the chatbot
    And user validates the chatbot output
    And user makes the call to chat base for Analytics

    #Ensure that the json has the conversation in the BotDAO format
    #Mention the file name of the file
  @JsonInput
  Scenario: To input Texts through Json
    Given  user navigates to the test bot url
    Then user records the chat input from Json input
    And user inputs the values in the chatbot
    And user validates the chatbot output
    And user makes the call to chat base for Analytics

    @TextInput
    Scenario: To input Texts through Text
      Given  user navigates to the test bot url
      Then user records the chat input from Text input input
      And user inputs the values in the chatbot
      And user validates the chatbot output
      And user makes the call to chat base for Analytics
      And end of test case