package StepDefs;

import Core.Environment;
import Pages.BasePage;
import Utils.MyVariables;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 590256 on 3/20/2018.
 */
public class BasicStepDefs {
    private final Logger logger = Logger.getLogger("StoLog");

    private Environment environment;
    EventFiringWebDriver driver;
    BasePage bPage;
    ArrayList<String> chatResponseHistory;
    int count =1;
    public BasicStepDefs() {
        this.environment = new Environment("Chrome");
        this.driver = this.environment.getBrowserDriver();
        bPage = new BasePage(driver);
        chatResponseHistory = new ArrayList<>();

    }

    @Given("^user hits the base chat bot URL$")
    public void user_hits_the_base_chat_bot_URL() throws Throwable {
        System.out.println(driver.getWindowHandle());
        this.driver.get(MyVariables.BOTURL);
        Thread.sleep(10000L);
        System.out.println(bPage.waitForPageLoad());
        //System.out.println(bPage.getLatestReplyFromBOT());
        System.out.println(bPage.GetConvLength());
    }

    @When("^user enters data into the input (.*) box$")
    public void user_enters_data_into_the_input_box(String input) throws Throwable {
        bPage.enterText(input);

    }

    @And("^user hits enter$")
    public void user_hits_enter() throws Throwable {
        bPage.hitEnter();
    }

    @Then("^user receives a response back with (.*)$")
    public void user_receives_a_response_back(String expected) throws Throwable {
        Thread.sleep(3000L);
        System.out.println(bPage.GetConvLength());
        Assert.assertTrue(bPage.GetConvLength()>2);
        System.out.println("BOT Reply : "+bPage.getLatestReplyFromBOT());
        Assert.assertEquals(bPage.getLatestReplyFromBOT(),expected,"Wrong Response returned From BOT");

    }

    @When("^user enters the following data and hits enter$")
    public void user_enters_the_following_data_and_hits_enter(List<String> questions) throws Throwable {
        logger.trace("Question Posted!");
        for(String qs:questions){
            bPage.enterText(qs);
            Thread.sleep(2000L);
            bPage.hitEnter();
            count++;
            //Add a check to see if conversation count is updated
            Thread.sleep(2000L);
            if(bPage.GetConvLength()>count){
                chatResponseHistory.add(bPage.getLatestReplyFromBOT());
            }

        }
    }

    @Then("^user receives the following response back$")
    public void user_receives_the_following_response_back(List<String> expectedResponse) throws Throwable {
        logger.trace("Validating the Response");
        Assert.assertEquals(chatResponseHistory,expectedResponse,"Chat behaviour error");
    }

    @Then("^user receives the following response back Relaxed check$")
    public void user_receives_the_following_response_back_Relaxed(List<String> expectedResponse) throws Throwable {
        logger.trace("Validating the Response");

        for(int i=0;i<chatResponseHistory.size();i++){
            Assert.assertTrue(expectedResponse.get(i).contains(chatResponseHistory.get(i)));
        }
    }

    @Then("^the below chat history is recorded$")
    public void the_below_chat_history_is_recorded(Map<String,String> QA) throws Throwable {
        logger.trace("Recording chat history");
        for(Map.Entry entry : QA.entrySet()){
            System.out.println("Question:"+entry.getKey().toString());
            System.out.println("ExpectedAnswer:"+entry.getValue().toString());
            //check to see if key is not null
            bPage.enterText(entry.getKey().toString().trim());
            bPage.hitEnter();
            Thread.sleep(2000L);
            Assert.assertTrue(entry.getValue().toString().contains(bPage.getLatestReplyFromBOT()));

        }


    }

    @Then("^the below chat history is recorded for users$")
    public void the_below_chat_history_is_recorded_for_users(Map<String,List<String>> QA) throws Throwable {
        logger.trace("Recording chat history with strict check");
        for(Map.Entry entry:QA.entrySet()){
            System.out.println("Question:"+entry.getKey().toString());
            bPage.enterText(entry.getKey().toString().trim());
            bPage.hitEnter();
            Thread.sleep(2000L);

        }
    }


}
