package StepDefs;

import Core.Environment;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/**
 * Created by 590256 on 4/3/2018.
 */
public class Hooks{

    private final Logger logger = Logger.getLogger("StoLog");
    Environment environment;
    EventFiringWebDriver driver;


    @Before
    public void setUp() throws Exception {
        logger.info("&&&&+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++&&&&");
        System.out.println("Before");

    }

    @After
    public  void tearDown() throws Exception {
        logger.info("\nExecution Complete");
        System.out.println("After");

    }

}
