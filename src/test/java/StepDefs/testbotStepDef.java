package StepDefs;

import Core.BotDAOCopy;
import Core.Environment;
import Core.RequestBody;
import Pages.RequestBuilder;
import Pages.testBotPage;
import Utils.JsonParser;
import Utils.MyVariables;
import Utils.RestPostHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 590256 on 4/9/2018.
 */
public class testbotStepDef {
    private final Logger logger = Logger.getLogger("StoLog");
    private Environment environment;
    EventFiringWebDriver driver;
    testBotPage tPage;
    List<String> expectedUserConvo;
    List<String> expectedBotConvo;
    List<BotDAOCopy> chatHistories;
    Hooks hook;

    public testbotStepDef() {
        this.environment = new Environment("Chrome");
        this.driver = this.environment.getBrowserDriver();
        tPage = new testBotPage(driver);
        expectedUserConvo = new ArrayList<>();
        expectedBotConvo = new ArrayList<>();
        chatHistories = new ArrayList<>();

    }

    @Given("^user navigates to the test bot url$")
    public void user_navigates_to_the_test_bot_url() throws Throwable {
        this.driver.get(MyVariables.TESTBOT);
        this.driver.switchTo().frame("frame");
        Assert.assertTrue(tPage.waitForPageLoad());

    }

    @When("^user has sends a (.*)$")
    public void user_has_sends_a_hi(String input) throws Throwable {
        tPage.enterText(input);
        tPage.hitEnter();
        Assert.assertTrue(tPage.getUserInput().size() > 0);
    }

    @Then("^bot replies back$")
    public void bot_replies_back() throws Throwable {
        Assert.assertTrue(!tPage.getLastBotReply().isEmpty(), "BOT NOT RESPONDING!!");
        Assert.assertTrue(tPage.getBotOutput().size() > 0, "No REPLY FROM BOT");

    }

    @Then("^user has the following conversation with Bot$")
    public void user_has_the_following_conversationTwo(List<String> chat) throws Throwable {
        BotDAOCopy botDao = new BotDAOCopy();
        chatHistories = botDao.botDataFormatter(chat);
        for (BotDAOCopy keys : chatHistories) {
            logger.info("===========");
            logger.info(keys.getUserquestion());
            logger.info("----------------------");
            logger.info(keys.getExpectedBotResponse().toString());
        }
    }

    @And("^user inputs the values in the chatbot$")
    public void user_inputs_the_values_in_the_chatbot() throws Throwable {
        logger.trace("Conversation begins with the Chat bot");
        for (BotDAOCopy keys : chatHistories) {
            tPage.enterText(keys.getUserquestion());
            tPage.hitEnter();
            Thread.sleep(500L);
        }

    }

    @And("^user validates the chatbot output$")
    public void user_validates_the_chatbot_output() throws Throwable {
        logger.trace("Validating the bot conversation");
        for (int i = 0; i < tPage.getBotOutput().size(); i++) {
            logger.info("Expected:" + chatHistories.get(i).getExpectedBotResponse());
            logger.info("Actual:" + tPage.getBotOutput().get(i));
            logger.info("-----------------ASSERT-----------");
            Assert.assertTrue(chatHistories.get(i).getExpectedBotResponse().contains(tPage.getBotOutput().get(i)));
        }
    }


    @And("^user makes the call to chat base for Analytics$")
    public void user_makes_the_call_to_chat_base_for_Analytics() throws Throwable {
        List<RequestBody> userConvo = RequestBuilder.userRequestBuild(this.tPage);
        List<RequestBody> botConvo = RequestBuilder.botRequestBuild(this.tPage);
        //posting the request object
        RestPostHelper.postRequestBody(userConvo, botConvo);
    }

    @Then("^user records the chat input from Json (.*)$")
    public void user_records_the_chat_input_from_Json(String fileName) throws Throwable {
        logger.trace("Reading from file name:   " + fileName + ".json");
        BotDAOCopy botDao = new BotDAOCopy();
        chatHistories = botDao.botDataFormatter(JsonParser.convertInputFileToValueObject(fileName));
        for (BotDAOCopy keys : chatHistories) {
            logger.info("===========");
            logger.info(keys.getUserquestion());
            logger.info("----------------------");
            logger.info(keys.getExpectedBotResponse().toString());
        }
    }

    @Then("^user records the chat input from Text input (.*)")
    public void user_records_the_chat_input_from_Text_input_input(String fileName) throws Throwable {
        logger.trace("Reading from text File:   " + fileName + ".txt");
        BotDAOCopy botDao = new BotDAOCopy();
        chatHistories = botDao.botDataFormatter(JsonParser.convertInputTextFileToValueObject(fileName));
        for (BotDAOCopy keys : chatHistories) {
            logger.info("===========");
            logger.info(keys.getUserquestion());
            logger.info("----------------------");
            logger.info(keys.getExpectedBotResponse().toString());
        }
    }

    @And("^end of test case$")
    public void end_of_test_case() throws Throwable {
        logger.trace("END OF TEST CASE");
        this.driver.quit();
    }
}
