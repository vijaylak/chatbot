package Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 590256 on 4/9/2018.
 */
public class JsonParser {
    private static final Logger logger = Logger.getLogger("StoLog");


    public static String ObjectToString(Object object) {
        Gson gson = new GsonBuilder().create();
        String jsonString = gson.toJson(object);

        return jsonString;
    }

    public static Object StringToObject(String string, Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = mapper.readValue(string, object.getClass());
        return obj;
    }

    public static Object StringToObject(String string, Class cls) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = mapper.readValue(string, cls);
        return obj;
    }


    public static String printValueObject(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        // mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        String result = mapper.writeValueAsString(object);

        return result;
    }

    public static List<String> convertInputFileToValueObject(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("resources/InputJson/" + fileName + ".json"));
        String text;
        String temp = "";
        List<String> history = new ArrayList<>();
        while ((text = br.readLine()) != null) {
            if (text.contains("{") || text.contains("}")) {
                continue;
            }
            temp = text.replace("\"", "");
            System.out.println(temp);
            history.add(temp.substring(0,temp.length()-1).trim());

        }

        logger.trace("Read from File:"+history);

        return history;
    }

    public static List<String> convertInputTextFileToValueObject(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("resources/InputText/" + fileName + ".txt"));
        String text;
        List<String> history = new ArrayList<>();
        while ((text = br.readLine()) != null) {
            history.add(text.trim());
        }
        System.out.println(history);
        return history;

    }

 /*   @Test()
    public void sampler() throws IOException {
        String fileName = "input";
        String text;
        String temp = "";
        List<String> history = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("resources/InputJson/" + fileName + ".json"));
        while ((text = br.readLine()) != null) {
            if (text.contains("{") || text.contains("}")) {
                continue;
            }
            temp = text.replace("\"","");
            System.out.println(temp);
            System.out.println("-------->"+temp.substring(0,temp.length()-1));
            history.add(temp.trim());

        }


        System.out.println(history);
        // System.out.println(br.readLine());
    }*/
}
