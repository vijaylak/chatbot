package Utils;

import Core.RequestBody;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

/**
 * Created by 590256 on 4/9/2018.
 */
public class RestPostHelper {
    private static Logger logger = Logger.getLogger("StoLog");

    public static void postRequestBody(List<RequestBody> userConvo, List<RequestBody> botConvo) throws IOException {
        logger.trace("Posting objects");
        if (userConvo.size() == botConvo.size()) {
            for (int i = 0; i < userConvo.size(); i++) {
                logger.trace("-----------");
                logger.trace(JsonParser.ObjectToString(botConvo.get(i)));
                Assert.assertEquals(poster(JsonParser.ObjectToString(userConvo.get(i))), 200);

                Assert.assertEquals(poster(JsonParser.ObjectToString(userConvo.get(i))), 200);


            }


        }
    }

    private static int poster(String body) throws IOException {
        logger.trace("$$$$$$$$$$$$$$$$");
        logger.trace(body);
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(MyVariables.EndPoint);
        request.setHeader("Content-Type", MyVariables.JSON);
        request.setEntity(new StringEntity(body));
        HttpResponse response = client.execute(request);
        logger.trace(response.getStatusLine().getStatusCode());
        logger.trace(response.getStatusLine().getReasonPhrase());
        return response.getStatusLine().getStatusCode();
    }
}
