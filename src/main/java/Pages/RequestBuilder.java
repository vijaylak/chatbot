package Pages;

import Core.RequestBody;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 590256 on 4/14/2018.
 */
public class RequestBuilder {

    private static final Logger logger = Logger.getLogger("StoLog");

    /**
     * Below Method is used to build the request Objects for Chatbase Analytics
     */

    public static List<RequestBody> userRequestBuild(testBotPage tPage){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long times = timestamp.getTime();
        List<RequestBody> userConvo = new ArrayList<>();

        for (String userInt : tPage.getUserInput()) {
            RequestBody temp = new RequestBody("user", userInt, times);
            times += 10;
            userConvo.add(temp);
        }

        return userConvo;
    }

    public static List<RequestBody> botRequestBuild(testBotPage tPage){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long times = timestamp.getTime();
        List<RequestBody> botConvo = new ArrayList<>();

        for (String botOut : tPage.getBotOutput()) {
            times += 11;
            RequestBody temp = new RequestBody("agent", botOut, times);

            botConvo.add(temp);
        }

        return botConvo;
    }

}
