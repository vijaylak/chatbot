package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

/**
 * Created by 590256 on 3/20/2018.
 */
public class BasePage extends AbstractPage {
    private WebDriver driver;

    @FindBy(css = "#chatBoxHeader")
    private WebElement chatBotHeader;

    @FindBy(css = "#text-field")
    private WebElement inputBox;
    //#chatbox > div:nth-child(4)
    //#chatbox > div:nth-child(2)
    //#chatText
    //#chatbox > div:nth-child(3)
    //#chatbox > div:nth-child(5)
    //#chatbox > div:nth-child(1)

    @FindBy(css = "#chatbox")
    private WebElement chatBox;

/*    @FindBy(id = "chatText")
    private WebElement chatText;*/
    By chatText = By.id("chatText");

    @FindBy(css = "#textHeader")
    private WebElement testHeader;

    public BasePage(WebDriver driver) {
        super(driver);
        System.out.println(driver.getWindowHandle());
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 40),
                this);
    }

    @Override
    public  boolean waitForPageLoad() {
        boolean isPageLoaded = false;
        System.out.println(driver.getWindowHandle());
        try {
            waitTillElementIsVisible(this.inputBox);
            isPageLoaded = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isPageLoaded;
    }

    public int GetConvLength(){
        List<WebElement> chatConvo = this.chatBox.findElements(chatText);
        return chatConvo.size();
    }

    public String getLatestReplyFromBOT(){
        List<WebElement> chatConvo = this.chatBox.findElements(chatText);
        String reply = "";
        if(chatConvo.size()>1){
            reply = chatConvo.get(chatConvo.size()-1).getText();
        }

        return reply;
    }

    public void enterText(String input){
        enterText(inputBox,input);
    }

    public void hitEnter(){
        this.inputBox.sendKeys(Keys.RETURN);
    }


}
