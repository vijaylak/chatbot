package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 590256 on 4/9/2018.
 */
public class testBotPage extends AbstractPage {
    private WebDriver driver;

    @FindBy(css = "#query")
    private WebElement input;

    @FindBy(css = "#result")
    private WebElement chatHistory;

    public testBotPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 40),
                this);
    }

    @Override
    public  boolean waitForPageLoad() {
        boolean isPageLoaded = false;
        System.out.println(driver.getWindowHandle());
        try {
            waitTillElementIsVisible(this.input);
            isPageLoaded = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isPageLoaded;
    }

    //Method to retrieve user input
    public List<String> getUserInput(){
        List<WebElement> userInputs = this.chatHistory.findElements(By.className("user-request"));
        List<String> userConvo = new ArrayList<>();
        for(WebElement ele:userInputs){
            userConvo.add(ele.getText().trim());
        }

        return userConvo;
    }
    //Method to retrieve bot output
    public List<String> getBotOutput(){
        List<WebElement> botOutput = this.chatHistory.findElements(By.className("server-response"));
        List<String> botConvo = new ArrayList<>();
        for(WebElement ele:botOutput){
            botConvo.add(ele.getText().trim());
        }

        return botConvo;
    }

    public String getLastBotReply(){
        List<String> botConvo = getBotOutput();
        return botConvo.get(botConvo.size()-1);
    }

    public String getLastUserInput(){
        List<String> userInput = getUserInput();
        return userInput.get(userInput.size()-1);
    }

    public void enterText(String input){
        enterText(this.input,input);
    }

    public WebElement getInput() {
        return input;
    }

    public void hitEnter(){
        this.input.sendKeys(Keys.RETURN);
    }
}
