package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by 590256 on 3/20/2018.
 */
public abstract class AbstractPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public AbstractPage(WebDriver driver) {

        this.driver = driver;
        System.out.println(driver.getWindowHandle());
        wait = new WebDriverWait(driver,50);
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    public WebElement waitTillElementIsVisible(WebElement element){
        return wait.until(ExpectedConditions.visibilityOf(element));
    }


    public WebElement waitTillElementBecomeVisible(By locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public List<WebElement> waitTillAllElmentsAreVisible(List<WebElement> elements) {
        return wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public void waitTillElementPresent(String element) {

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(element)));
    }

    public WebElement waitTillElementIsClickable(WebElement element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void enterText(WebElement element, String input) {
        waitTillElementIsVisible(element).clear();
        System.out.println("sending keys " + input);
        waitTillElementIsClickable(element).sendKeys(input);

    }

    public void click(WebElement element) {
        waitTillElementIsClickable(element).click();

    }
    public boolean selectOption(WebElement element, String option) {

        Select dropDown = new Select(element);
        dropDown.selectByVisibleText(option);
        return dropDown.getFirstSelectedOption().isSelected();
    }

    public boolean selectOption(WebElement element, int option) {

        Select dropDown = new Select(element);
        dropDown.selectByIndex(option);
        return dropDown.getFirstSelectedOption().isSelected();
    }

    public boolean selectOptionByValue(WebElement element, String option) {

        Select dropDown = new Select(element);
        dropDown.selectByValue(option);
        return dropDown.getFirstSelectedOption().isSelected();
    }

    public static void pageScrollDown(WebDriver driver){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }

    public static void pageBottomScroll(WebDriver driver){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    protected abstract boolean waitForPageLoad();

}
