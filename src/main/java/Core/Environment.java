package Core;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/**
 * Created by 590256 on 3/20/2018.
 */
public class Environment {
//https://lukebot.aexp.com/
    // need a browser with properties altered
    EventFiringWebDriver browserDriver = null;
    private String root = System.getProperty("user.dir");

    public Environment(String browserName){
        this.browserDriver = getBrowserProp(browserName);
    }

    private EventFiringWebDriver getBrowserProp(String browserName){
        EventFiringWebDriver driver = null;
        switch (browserName.toLowerCase()){
            case "chrome":
               driver = this.getChromeLocal();
                break;
            default:
                System.out.println("No Browser Name Specified!");

        }
        return driver;
    }

    private EventFiringWebDriver getChromeLocal(){

        WebDriver chrome = null;
        try{
            System.setProperty("webdriver.chrome.driver", root+"/resources/chromedriver.exe");

            ChromeOptions options = new ChromeOptions();
            options.addArguments("test-type");
            options.addArguments("start-maximized");
            options.addArguments("--enable-automation");
            options.addArguments("test-type=browser");
            options.addArguments("disable-infobars");

            Proxy proxy = new Proxy();
            proxy.setProxyType(Proxy.ProxyType.MANUAL);
            proxy.setHttpProxy("http://proxy-phoenix.aexp.com" + ":" + 8080);
            proxy.setSslProxy("http://proxy-phoenix.aexp.com" + ":" + 8080);
            proxy.setFtpProxy("http://proxy-phoenix.aexp.com" + ":" + 8080);


            DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
            desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);
            desiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);


            chrome = new ChromeDriver(options);



        }catch (Exception e){
            e.printStackTrace();
        }

        return new EventFiringWebDriver(chrome);
    }

    public EventFiringWebDriver getBrowserDriver() {
        return browserDriver;
    }
}
