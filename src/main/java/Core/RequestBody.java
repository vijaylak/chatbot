package Core;

import Utils.MyVariables;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by 590256 on 4/9/2018.
 */
public class RequestBody {
    @JsonProperty(value = "api_key")
    private String api_key;

    @JsonProperty(value = "type")
    private String type;
    @JsonProperty(value = "user_id")
    private String user_id;
    @JsonProperty(value = "time_stamp")
    private long time_stamp;
    @JsonProperty(value = "platform")
    private String platform;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "intent")
    private String intent;

    @JsonProperty(value = "version")
    private String version;

    @JsonProperty(value = "session_id")
    private String session_id;

    public RequestBody() {

    }

    public RequestBody(String type,String message,long time_stamp){
        this.api_key = MyVariables.API_KEY;
        this.type = type;
        this.user_id = MyVariables.user_ID;
        this.time_stamp = time_stamp;
        this.platform = "Actions";
        this.message = message;
        this.intent = MyVariables.intent;
        this.version = MyVariables.ver;
        this.session_id = MyVariables.session;
    }

    public RequestBody(String api_key, String type, String user_id, long time_stamp, String platform, String message, String intent, String version, String session_id) {
        this.api_key = api_key;
        this.type = type;
        this.user_id = user_id;
        this.time_stamp = time_stamp;
        this.platform = platform;
        this.message = message;
        this.intent = intent;
        this.version = version;
        this.session_id = session_id;
    }

    public String getApi_key() {

        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public long getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(long time_stamp) {
        this.time_stamp = time_stamp;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
}
