package Core;

import java.util.List;

/**
 * Created by 590256 on 4/14/2018.
 */
public class BotList {
    List<BotDAOCopy> chatHistory;

    public BotList(List<BotDAOCopy> chatHistory) {
        this.chatHistory = chatHistory;
    }

    public BotList() {
    }

    public List<BotDAOCopy> getChatHistory() {
        return chatHistory;
    }

    public void setChatHistory(List<BotDAOCopy> chatHistory) {
        this.chatHistory = chatHistory;
    }
}
