package Core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 590256 on 4/13/2018.
 */
public class BotDAOCopy {
    public String userRep = "User:";
    public String botRep = "Agent:";
    public String userquestion;
    public ArrayList<String> expectedBotResponse = new ArrayList<String>();


    public ArrayList<BotDAOCopy> botDataFormatter(List<String> cucumberInput) {

        ArrayList<BotDAOCopy> botArray = new ArrayList<BotDAOCopy>();


        Iterator iterator = cucumberInput.iterator();
        BotDAOCopy botdataobject=null;
        while (iterator.hasNext()){

            String input =iterator.next().toString();

            if ((input.indexOf(userRep) == 0)) {

                botdataobject = new BotDAOCopy();
                botdataobject.userquestion = input.substring(userRep.length());
                botArray.add(botdataobject);

            }

            if (input.indexOf(botRep) == 0) {
                botdataobject.expectedBotResponse.add(input.substring(botRep.length()));
            }

        }


        return botArray;
    }

    public String getUserquestion() {
        return userquestion;
    }

    public void setUserquestion(String userquestion) {
        this.userquestion = userquestion;
    }

    public ArrayList<String> getExpectedBotResponse() {
        return expectedBotResponse;
    }

    public void setExpectedBotResponse(ArrayList<String> expectedBotResponse) {
        this.expectedBotResponse = expectedBotResponse;
    }
}
